/*
 * Cassie Chin
 * Lab 8, Question 3
 *
 * Creates permutations of an unknown number of strings of various sizes while removing duplicated permutaions.
 *
 * Example:
 *
 * seed
 * 
 * seed sede sdee esed esde eesd eeds edes edse dees dese dsee
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "results.h"

// Length of a line in the input file (which is a single word)
#define MAX_LINE_LENGTH 32

// Max number of permutation results
#define MAX_NUM_RESULTS 256

// Store the results in here
Results *results;

/**
 * Function to swap values at two pointers
 * @param x First string to swap
 * @param y Second string to swap
 */
void swap (char *x, char *y) {
  char temp;
  temp = *x;
  *x = *y;
  *y = temp;
}

/**
 * GeeksForGeeks implementation of wolfram's permutation algorithm.
 * (http://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/)
 *
 * Function to print permutations of string
 * This function takes three parameters:
 * @param a String
 * @param i Starting index of the string
 * @param n Ending index of the string. */
void permute(char *a, int i, int n) {
  int j;
  if (i == n) Results_add (results, a);
  else {
    for (j = i; j <= n; j++) {
      swap((a+i), (a+j));
      permute(a, i+1, n);
      swap((a+i), (a+j)); //backtrack
    }
  }
} 

/**
 * Parses the words in a buffer and stores the pairs of each word.
 *
 * @param buffer The buffer containing the words. The assignment file input specifies that the first thing in the 
 *               buffer will be an integer representing the number of words.
 */
void processLine (char *buffer) {
  //  printf("%s\n", buffer);
  permute (buffer, 0, strlen(buffer)-1);
  Results_print (results);
  Results_clear (results);
}

/**
 * Reads each line of an input file called 'permutewords'
 */
void main () {
  // The input file must be called "wordpairs"
  FILE *fp = fopen ("permutewords", "r");
  results = new_Results ();

  // The file doesn't exist so terminate the program
  if (!fp) return;

  // Read in a file line by line and store it in the string buffer
  char *buffer = (char *) calloc (MAX_LINE_LENGTH, sizeof(char));
  while (fgets (buffer, MAX_LINE_LENGTH, fp)) {
    buffer[strlen(buffer)-1] = '\0';

    // Each line in the input file is processed individually
    processLine(buffer);
  }
  
  // Clean up and return
  delete_Results (results);
  fclose(fp);
  return;
}

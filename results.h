#ifndef __RESULTS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// The maximum number of characters a word can be
#define MAX_WORD_LENGTH 32

// The maximum number of unique permutations a word can have
#define MAX_NUM_RESULTS 256

/*
 * Structure to hold the results of a single wod in.
 * The results array stores the strings.
 * The resultsIndex tells how many strings are in the array.
 */
typedef struct Results {
  char **results;
  int resultsIndex;
} Results;

/**
 * Creates a new Results object
 */
Results* new_Results ();

/**
 * Deletes a Results object
 */
void delete_Results (Results* self);

/**
 * Clear the results object by setting the index back to 0.
 */
void Results_clear (Results *self);

/**
 * Add a string to the structure if and only if it is a unique string.
 *
 * @param self The result structure to modify
 * @param newResult The string to add to the structure
 */
void Results_add (Results *self, char* newResult);

/**
 * Print out the string array in the Results object
 *
 * @param self The result structure to modify
 */
void Results_print (Results *self);

#endif

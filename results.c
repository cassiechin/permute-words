#include "results.h"

/**
 * Creates a new Results object
 */
Results* new_Results () {
  Results* self = (Results *) calloc (1, sizeof(Results));
  self->resultsIndex = 0;
  self->results = (char **) calloc (MAX_NUM_RESULTS, sizeof(char *));

  int i;
  for (i=0; i<MAX_NUM_RESULTS; i++) self->results[i] = (char *) calloc (MAX_WORD_LENGTH, sizeof(char));
  return self;
}

/**
 * Deletes a Results object
 */
void delete_Results (Results* self) {
  if (!self) return;

  int i;
  for (i=0; i<MAX_NUM_RESULTS; i++) free(self->results[i]);
  free (self->results);
  free (self);
}

/**
 * Clear the results object by setting the index back to 0.
 */
void Results_clear (Results *self) {
  if (!self) return;
  self->resultsIndex = 0;
}

/**
 * Add a string to the structure if and only if it is a unique string.
 *
 * @param self The results structure to modify
 * @param newResult The string to add to the structure
 */
void Results_add (Results *self, char* newResult) {
  if (!self) return;

  // If the string is already in the array, then return without doing anything
  int i;
  for (i=0; i<self->resultsIndex; i++) {
    if (strcmp(newResult, self->results[i]) == 0) return;
  }

  // The string was not found, so add it to the end of the array
  strcpy(self->results[self->resultsIndex++], newResult);
}

/**
 * Print out the string array in the Results object
 * Each string in the array is separated by a single space.
 *
 * @param self The result structure to modify
 */
void Results_print (Results *self) {
  if (!self) return;

  int i;
  for (i=0; i<self->resultsIndex; i++) {
    printf("%s ", self->results[i]);
  }
  printf("\n");
}
